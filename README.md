# pokemon-app
This is a poc using React / redux saga. The app allows users to see pokemens and some details about them

# Getting Started
Prerequisites : 
- Node.js (version 16.13.1 or higher)
- npm (version 8.1.0 or higher)

# Installing
- Clone the repository to your local machine
- Run npm install to install all dependencies
- Run npm start to start the development server

Open http://localhost:3000 in your browser to view the app

# Building
Run npm run build to create a production build of the app
