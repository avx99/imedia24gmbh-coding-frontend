/* eslint-disable react-hooks/exhaustive-deps */
import { useInjectReducer } from "../../app/injectReducer";
import { useInjectSaga } from "../../app/injectSaga";
import saga from "./saga";
import reducer, { initialState } from "./reducer";
import { pokemonsStore } from "../../app/applicationStates";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getPokemons, getPokemonsDetails } from "./action";
import Loader from "../../components/Loader";
import AlertPopup from "../../components/Alert";

import Grid from "@mui/material/Grid";
import PokemonCard from "../../components/Card";
import PokemonModal from "../../components/Modal";
import usePageBottom from "../../hooks/usePageBottom";

const key = pokemonsStore;
const size = 50;

export default function PokemonsContainer() {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const [page, setPage] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState([]);

  const dispatch = useDispatch();

  const pokemons =
    useSelector((state) => state?.[key]?.pokemons) || initialState?.pokemons;
  const pokemonDetails =
    useSelector((state) => state?.[key]?.pokemonsDetails) ||
    initialState?.pokemonsDetails;


  const fetchData = () => {
    dispatch(getPokemons({ limit: size, offset: page }));
    setPage(page + 1);
  };

  const onPokemonSelect = (url) => {
    const id = url?.split("pokemon/")?.slice(-1)?.[0]?.replace("/", "");
    dispatch(getPokemonsDetails(id));
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (!pokemonDetails?.loading && pokemonDetails?.success) {
      setOpenModal(true)
    }
  }, [pokemonDetails]);

  
  useEffect(() => {
    if (!pokemons?.loading && pokemons?.success) {
      setData([...data, ...pokemons?.data?.results])
    }
  }, [pokemons]);

  const reachedBottom = usePageBottom();

  useEffect(() => {
    if (reachedBottom) {
      fetchData()
    }
  }, [reachedBottom]);

  return (
    <>
      {(pokemons?.loading || pokemonDetails?.loading) && <Loader />}
      {pokemons?.error && (
        <AlertPopup
          type={"error"}
          message={"problem occured while fetching data"}
        />
      )}
      {openModal && pokemonDetails?.success && !pokemonDetails?.loading && (
        <PokemonModal pokemon={pokemonDetails?.data}/>
      )}
      <Grid
        container
        spacing={{ xs: 1, md: 3 }}
        columns={{ xs: 1, sm: 8, md: 12 }}
      >
        {data?.map((item) => (
          <PokemonCard
            pokemon={item}
            onClick={() => onPokemonSelect(item?.url)}
          />
        ))}
      </Grid>
    </>
  );
}
