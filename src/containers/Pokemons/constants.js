
export const WS_GET_POKEMONS = (limit, offest) => `pokemon?limit=${limit}&offset=${offest}`;
export const WS_GET_POKEMON_DETAILS = (id) => `pokemon/${id}/`;

export const GET_POKEMONS = 'imedia24/pokemons/GET_POKEMONS';
export const GET_POKEMONS_SUCCESS = 'imedia24/pokemons/GET_POKEMONS_SUCCESS';
export const GET_POKEMONS_ERROR = 'imedia24/pokemons/GET_POKEMONS_ERROR';

export const GET_POKEMON_DETAILS = 'imedia24/pokemons/GET_POKEMON_DETAILS';
export const GET_POKEMON_DETAILS_SUCCESS = 'imedia24/pokemons/GET_POKEMON_DETAILS_SUCCESS';
export const GET_POKEMON_DETAILS_ERROR = 'imedia24/pokemons/GET_POKEMON_DETAILS_ERROR';