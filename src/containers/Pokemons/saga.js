import { call, put, takeLatest } from "redux-saga/effects";

import instance from "../../app/request";
import {
  getPokemonsSuccess,
  getPokemonsError,
  getPokemonsDetailsSuccess,
  getPokemonsDetailsError,
} from "./action";
import {
  GET_POKEMONS,
  GET_POKEMON_DETAILS,
  WS_GET_POKEMONS,
  WS_GET_POKEMON_DETAILS,
} from "./constants";

export function* getPokemonsEmitter(action) {
  const requestURL = WS_GET_POKEMONS(
    action?.queryParams?.limit,
    action?.queryParams?.offset
  );
  try {
    const response = yield call(instance.get, requestURL);
    yield put(getPokemonsSuccess(response?.data));
  } catch (err) {
    yield put(getPokemonsError(err));
  }
}

export function* getPokemonDetailsEmitter(action) {
  const requestURL = WS_GET_POKEMON_DETAILS(action?.id);
  try {
    const response = yield call(instance.get, requestURL);
    yield put(getPokemonsDetailsSuccess(response?.data));
  } catch (err) {
    yield put(getPokemonsDetailsError(err));
  }
}

export default function* pokemonsHandler() {
  yield takeLatest(GET_POKEMONS, getPokemonsEmitter);
  yield takeLatest(GET_POKEMON_DETAILS, getPokemonDetailsEmitter);
}
