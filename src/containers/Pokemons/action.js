import {
  GET_POKEMONS,
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_ERROR,
  GET_POKEMON_DETAILS,
  GET_POKEMON_DETAILS_SUCCESS,
  GET_POKEMON_DETAILS_ERROR
} from "./constants";

export function getPokemons(queryParams) {
  return {
    type: GET_POKEMONS,
    queryParams
  };
}

export function getPokemonsSuccess(data) {
  return {
    type: GET_POKEMONS_SUCCESS,
    data,
  };
}

export function getPokemonsError(error) {
  return {
    type: GET_POKEMONS_ERROR,
    error,
  };
}

export function getPokemonsDetails(id) {
  return {
    type: GET_POKEMON_DETAILS,
    id
  };
}

export function getPokemonsDetailsSuccess(data) {
  return {
    type: GET_POKEMON_DETAILS_SUCCESS,
    data,
  };
}

export function getPokemonsDetailsError(error) {
  return {
    type: GET_POKEMON_DETAILS_ERROR,
    error,
  };
}
