import produce from "immer";
import {
  GET_POKEMONS,
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_ERROR,
  GET_POKEMON_DETAILS,
  GET_POKEMON_DETAILS_SUCCESS,
  GET_POKEMON_DETAILS_ERROR
} from "./constants";

// The initial state of the App
export const initialState = {
  pokemons: { loading: false, success: false, error: null, data: [] },
  pokemonsDetails: { loading: false, success: false, error: null, data: {} },
};

/* eslint-disable default-case, no-param-reassign */
const reducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_POKEMONS:
        draft.pokemons.loading = true;
        draft.pokemons.success = false;
        draft.pokemons.error = null;
        draft.pokemons.data = [];
        break;

      case GET_POKEMONS_SUCCESS:
        draft.pokemons.data = action.data;
        draft.pokemons.success = true;
        draft.pokemons.loading = false;
        draft.pokemons.error = null;
        break;

      case GET_POKEMONS_ERROR:
        draft.pokemons.error = action.error;
        draft.pokemons.success = false;
        draft.pokemons.loading = false;
        draft.pokemons.data = [];
        break;

      case GET_POKEMON_DETAILS:
        draft.pokemonsDetails.loading = true;
        draft.pokemonsDetails.success = false;
        draft.pokemonsDetails.error = null;
        draft.pokemonsDetails.data = {};
        break;

      case GET_POKEMON_DETAILS_SUCCESS:
        draft.pokemonsDetails.data = action.data;
        draft.pokemonsDetails.success = true;
        draft.pokemonsDetails.loading = false;
        draft.pokemonsDetails.error = null;
        break;

      case GET_POKEMON_DETAILS_ERROR:
        draft.pokemonsDetails.error = action.error;
        draft.pokemonsDetails.success = false;
        draft.pokemonsDetails.loading = false;
        draft.pokemonsDetails.data = {};
        break;
    }
  });

export default reducer;
