import {
    GET_POKEMONS,
    GET_POKEMONS_SUCCESS,
    GET_POKEMONS_ERROR,
    GET_POKEMON_DETAILS,
    GET_POKEMON_DETAILS_SUCCESS,
    GET_POKEMON_DETAILS_ERROR
  } from "../constants";
  import {
    getPokemons,
    getPokemonsSuccess,
    getPokemonsError,
    getPokemonsDetails,
    getPokemonsDetailsSuccess,
    getPokemonsDetailsError
  } from "../action";
  
  describe('Pokemons actions', () => {
    const sampleData = { name: 'Pikachu' };
    const sampleError = 'Error fetching data';
  
    it('should create an action to get pokemons', () => {
      const queryParams = { limit: 10, offset: 0 };
      const expectedAction = {
        type: GET_POKEMONS,
        queryParams
      };
      expect(getPokemons(queryParams)).toEqual(expectedAction);
    });
  
    it('should create an action to handle successful pokemon fetch', () => {
      const expectedAction = {
        type: GET_POKEMONS_SUCCESS,
        data: sampleData
      };
      expect(getPokemonsSuccess(sampleData)).toEqual(expectedAction);
    });
  
    it('should create an action to handle pokemon fetch error', () => {
      const expectedAction = {
        type: GET_POKEMONS_ERROR,
        error: sampleError
      };
      expect(getPokemonsError(sampleError)).toEqual(expectedAction);
    });
  
    it('should create an action to get pokemon details', () => {
      const id = 1;
      const expectedAction = {
        type: GET_POKEMON_DETAILS,
        id
      };
      expect(getPokemonsDetails(id)).toEqual(expectedAction);
    });
  
    it('should create an action to handle successful pokemon details fetch', () => {
      const expectedAction = {
        type: GET_POKEMON_DETAILS_SUCCESS,
        data: sampleData
      };
      expect(getPokemonsDetailsSuccess(sampleData)).toEqual(expectedAction);
    });
  
    it('should create an action to handle pokemon details fetch error', () => {
      const expectedAction = {
        type: GET_POKEMON_DETAILS_ERROR,
        error: sampleError
      };
      expect(getPokemonsDetailsError(sampleError)).toEqual(expectedAction);
    });
  });
  