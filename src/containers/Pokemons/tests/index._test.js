/* eslint-disable testing-library/prefer-screen-queries */
import React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import PokemonsContainer from './index';
import { initialState } from './reducer';
import { pokemonsStore } from '../../app/applicationStates';

const mockStore = configureStore([]);

describe('PokemonsContainer', () => {
  let store;
  beforeEach(() => {
    store = mockStore({
      [pokemonsStore]: initialState,
    });
  });

  it('renders Loader when loading', () => {
    const loadingState = {
      ...initialState,
      pokemons: { loading: true },
    };
    store = mockStore({ [pokemonsStore]: loadingState });

    const { getByTestId } = render(
      <Provider store={store}>
        <PokemonsContainer />
      </Provider>
    );

    expect(getByTestId('loader')).toBeInTheDocument();
  });
});