import { call, put } from "redux-saga/effects";
import { getPokemonsEmitter, getPokemonDetailsEmitter } from "../saga";
import {
  getPokemonsSuccess,
  getPokemonsError,
  getPokemonsDetailsSuccess,
  getPokemonsDetailsError,
} from "../action";
import instance from "../../../app/request";
import { WS_GET_POKEMONS, WS_GET_POKEMON_DETAILS } from "../constants";

jest.mock('axios');

describe("Pokemons Sagas", () => {
  describe("getPokemonsEmitter", () => {
    const queryParams = { limit: 10, offset: 0 };
    const action = { type: "GET_POKEMONS", queryParams };

    it("should dispatch getPokemonsSuccess on success", () => {
      const generator = getPokemonsEmitter(action);
      const mockResponse = { data: [{ name: "Pikachu" }] };

      instance.get.mockImplementation(() => Promise.resolve(mockResponse));
      expect(generator.next().value).toEqual(call(instance.get, WS_GET_POKEMONS(queryParams.limit, queryParams.offset)));
      
      expect(generator.next(mockResponse).value).toEqual(put(getPokemonsSuccess(mockResponse.data)));
      
      expect(generator.next().done).toBe(true);
    });

    it("should dispatch getPokemonsError on error", () => {
      const generator = getPokemonsEmitter(action);
      const mockError = new Error("Network error");
      instance.get.mockImplementation(() => Promise.resolve(mockError));

      expect(generator.next().value).toEqual(call(instance.get, WS_GET_POKEMONS(queryParams.limit, queryParams.offset)));
      expect(generator.throw(mockError).value).toEqual(put(getPokemonsError(mockError)));
      
      expect(generator.next().done).toBe(true);
    });
  });

  describe("getPokemonDetailsEmitter", () => {
    const id = 123;
    const action = { type: "GET_POKEMON_DETAILS", id };

    it("should dispatch getPokemonsDetailsSuccess on success", () => {
      const generator = getPokemonDetailsEmitter(action);
      const mockResponse = { data: { name: "Pikachu" } };
      instance.get.mockImplementation(() => Promise.resolve(mockResponse));

      expect(generator.next().value).toEqual(call(instance.get, WS_GET_POKEMON_DETAILS(id)));
      
      expect(generator.next(mockResponse).value).toEqual(put(getPokemonsDetailsSuccess(mockResponse.data)));
      
      expect(generator.next().done).toBe(true);
    });

    it("should dispatch getPokemonsDetailsError on error", () => {
      const generator = getPokemonDetailsEmitter(action);
      const mockError = new Error("Network error");
      instance.get.mockImplementation(() => Promise.resolve(mockError));
      expect(generator.next().value).toEqual(call(instance.get, WS_GET_POKEMON_DETAILS(id)));
      
      expect(generator.throw(mockError).value).toEqual(put(getPokemonsDetailsError(mockError)));
      
      expect(generator.next().done).toBe(true);
    });
  });
});
