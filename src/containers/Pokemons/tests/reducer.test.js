import reducer, { initialState } from '../reducer';
import {
  GET_POKEMONS,
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_ERROR,
  GET_POKEMON_DETAILS,
} from '../constants';

describe('Pokemons reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_POKEMONS', () => {
    const action = { type: GET_POKEMONS };
    const expectedState = {
      ...initialState,
      pokemons: { loading: true, success: false, error: null, data: [] }
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle GET_POKEMONS_SUCCESS', () => {
    const data = [{ name: 'Pikachu' }];
    const action = { type: GET_POKEMONS_SUCCESS, data };
    const expectedState = {
      ...initialState,
      pokemons: { loading: false, success: true, error: null, data }
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle GET_POKEMONS_ERROR', () => {
    const error = 'Error fetching data';
    const action = { type: GET_POKEMONS_ERROR, error };
    const expectedState = {
      ...initialState,
      pokemons: { loading: false, success: false, error, data: [] }
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle GET_POKEMON_DETAILS', () => {
    const action = { type: GET_POKEMON_DETAILS };
    const expectedState = {
      ...initialState,
      pokemonsDetails: { loading: true, success: false, error: null, data: {} }
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });
});
