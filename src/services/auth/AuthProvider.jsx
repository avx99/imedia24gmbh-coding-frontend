
import { isLogged } from "./authService"

// the auth provider should have a login component (<Login config={config}/>), and 
// he is gonna redirect you to it in case you are authenticated

function AuthProvider({children , config}) {
  return isLogged() ? children : <div config={config}>Not authorized</div>
}


export default AuthProvider