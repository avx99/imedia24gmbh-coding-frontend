import { Router, Switch, Route } from "react-router-dom";

import history from "./app/history";
import PokemonsPage from "./pages/Pokemons/PokemonsPage";

function App() {

  return (
    <Router history={history}>
      <div className="container">
        <Switch>
          <Route exact path="/">
            <PokemonsPage/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
