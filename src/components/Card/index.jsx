import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

export default function PokemonCard({ pokemon, onClick }) {
  return (
    <Grid item xs={1} sm={4} md={4}>
      <Card sx={{ width: 275, cursor: "pointer" }} onClick={onClick}>
        <CardContent>
          <Typography variant="h5" component="div">
            {pokemon?.name}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
}
