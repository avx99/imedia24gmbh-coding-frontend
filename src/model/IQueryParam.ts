export interface IQueryParam {
    limit: string;
    offset: string;
  }